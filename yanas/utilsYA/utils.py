import os
import time

class Canvas:
    
    def __init__(self):
        
        """Canvas class
        """
        
        super().__init__()
        self.canvasMap = []
        self.fillObj = "⬜"
    
    def clear(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def createCanvas(self,w = 10,h = 10,c = "⬜"):
        self.clear()
        for i in range(h):
            row = []
            for j in range(w):
                row.append(c)
            self.canvasMap.append(row)

        for i in range(0,h):
            print(c*w)


    def text(self,text,x,y):
        for i in range(0,len(text)):
            if x+i <= len(self.canvasMap[y])-1:
                self.canvasMap[y][x+i] = " " + text[i]

    def updateCanvas(self,fps = 60):
        self.clear()
        for i in range(0,len(self.canvasMap)):
            row = ""
            for j in range(0,len(self.canvasMap[i])):
                row = row + self.canvasMap[i][j]
                
            print(row)
        time.sleep(1/fps)

    def fill(self,c):
        
        self.fillObj = c

    def noFill(self):
        self.fillObj = "⬜"


    def wait(self,s):
        time.sleep(s)


    def rect(self,x,y,w,h = None,fill = True,limit = True):
        if h == None: 
            h=w

        if not fill:
            self.fillObj = "⬜"



        if not limit: 
            """WORK IN PROGRESS """
            if y > len(self.canvasMap)-1:
                y = y % len(self.canvasMap)-1
            elif y < 0:
                y = y % len(self.canvasMap)-1

            if x>len(canvasMap[y])-1:
                x = x % len(self.canvasMap[0])-1
            elif x<0:
                x = x % len(self.canvasMap[0])-1

            print(x,y)

            for i in range(y,y+h):
                for j in range(x,x+w):
                    self.canvasMap[i][j] = self.fillObj
        else:
            for i in range(y,y+h):
                if i <= len(self.canvasMap)-1 and i>=0:
                    for j in range(x,x+w):
                        if j <= len(self.canvasMap[y])-1 and j>=0:
                            self.canvasMap[i][j] = self.fillObj

    

    



    