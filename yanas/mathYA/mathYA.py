import math


def isPrime(n):
    for i in range(2,n):
        if n%i == 0:
            return False
    return True
    
def gcd(a,b):
    r = 1 
    while r != 0:
        r = a % b
        a = b
        b = r
    return a
    
def lcm(a,b):
    
    return int((a * b) / gcd(a,b))

def isIrred(num,den):
    for i in range(2,den):
        if num%i == 0 and den%i == 0:
            return False
    return True

def reduc(num,den):
    if not isIrred(num,den):
        for i in range(2,den):
            if num%i == 0 and den%i == 0:
                return num/i,den/i
    else:
        return None

def isNum(test):
    try:
        int(test)
    except Exception:
        return False
    
    return True

def cos_ah(dx, hypotenuse):
    """
    Calculate cosinus with "dx" and "hypotenuse"
    
    dx = x2 -x1
        
    """
    
    return dx/hypotenuse
    

def sin_oh(dy,hypotenuse):
    """
    Calculate cosinus with "dy" and "hypotenuse"
    
    dy = y2 -y1
        
    """
    
    return dy/hypotenuse
    
def tan_oa(dx,dy):
    return dy/dx


    

# def acos(x):
#     return 2*atan(x/(1 + math.sqrt(1 - x*x)))

# def asin():
#     pass
    
# def atan(X):
#     A = 1.0 / math.sqrt(1.0 + (X * X))
#     B = 1.0
#     N = 1
#     while N<=11:
#         A = (A + B) / 2.0
#         B = math.sqrt(A * B)
#         N = N + 1
#     return X / (math.sqrt(1.0 + (X * X)) * A)

#  def atan2():
#     pass


if __name__ == "__main__":
    pass

